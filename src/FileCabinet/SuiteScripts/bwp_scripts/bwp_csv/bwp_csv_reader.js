/**
 * @NApiVersion 2.1
 */
define(["require", "exports", "./bwp_csv_handler"], function (require, exports, bwp_csv_handler_1) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.CsvReader = void 0;
    const quotes = ['`', '"', '\''];
    class CsvReader extends bwp_csv_handler_1.CsvHandler {
        constructor(file, separator) {
            super(file, separator);
            this.storedLength = -1;
            let header;
            file.lines.iterator().each(({ value }) => {
                header = this.splitWithQuotes(value);
                return false;
            });
            this.header = header;
        }
        get width() {
            return this.header.length;
        }
        get length() {
            if (this.storedLength < 0) {
                this.storedLength = 0;
                this.file.lines.iterator().each(_line => {
                    this.storedLength++;
                    return true;
                });
            }
            return this.storedLength;
        }
        splitWithQuotes(line) {
            let values = [];
            let currentValue = '';
            let currentQuote = null;
            for (let i = 0; i < line.length; i++) {
                let char = line.charAt(i);
                if (currentValue.length === 0) {
                    if (quotes.includes(char)) {
                        currentQuote = char;
                    }
                    else if (char === this.separator) {
                        values.push(currentValue);
                        currentValue = '';
                    }
                    else {
                        currentValue += char;
                    }
                }
                else {
                    if (char === currentQuote) {
                        currentQuote = null;
                    }
                    else if (!currentQuote && char === this.separator) {
                        values.push(currentValue);
                        currentValue = '';
                    }
                    else {
                        currentValue += char;
                    }
                }
            }
            values.push(currentValue);
            return values;
        }
        getValue(colName, line) {
            let cell = line[this.header.indexOf(colName)];
            if (cell.startsWith('"') && cell.endsWith('"')) {
                cell = cell.substring(1, cell.length - 1);
            }
            return cell;
        }
        run(lineHandlers) {
            let n = 0;
            let continueFlags = Array.from({ length: lineHandlers.length }).fill(true);
            this.file.lines.iterator().each(({ value }) => {
                for (let i = 0; i < lineHandlers.length; i++) {
                    let lineHandler = lineHandlers[i];
                    if (continueFlags[i]
                        && (!lineHandler.start || n >= lineHandler.start)
                        && (!lineHandler.end || n < lineHandler.end)) {
                        continueFlags[i] = lineHandler.handler(this, this.splitWithQuotes(value));
                    }
                }
                n++;
                return continueFlags.reduce((prev, curr) => prev || curr);
            });
        }
    }
    exports.CsvReader = CsvReader;
});
