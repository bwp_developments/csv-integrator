/**
 * @NApiVersion 2.1
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(["require", "exports", "N/runtime", "./bwp_csv_integrator", "./bwp_csv_params"], function (require, exports, runtimeModule, bwp_csv_integrator_1, bwp_csv_params_1) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.execute = void 0;
    let execute = (_scriptContext) => {
        let hostKey = runtimeModule.getCurrentScript().getParameter({ name: bwp_csv_params_1.CUSTSCRIPT_BWP_SFTP_HOSTKEY });
        let url = runtimeModule.getCurrentScript().getParameter({ name: bwp_csv_params_1.CUSTSCRIPT_BWP_SFTP_URL });
        let username = runtimeModule.getCurrentScript().getParameter({ name: bwp_csv_params_1.CUSTSCRIPT_BWP_SFTP_USERNAME });
        let secret = runtimeModule.getCurrentScript().getParameter({ name: bwp_csv_params_1.CUSTSCRIPT_BWP_SFTP_SECRET });
        let hostKeyType = runtimeModule.getCurrentScript().getParameter({ name: bwp_csv_params_1.CUSTSCRIPT_BWP_SFTP_HOSTKEYTYPE });
        let port = runtimeModule.getCurrentScript().getParameter({ name: bwp_csv_params_1.CUSTSCRIPT_BWP_SFTP_PORT });
        let mappingId = runtimeModule.getCurrentScript().getParameter({ name: bwp_csv_params_1.CUSTSCRIPT_BWP_MAPPING_ID });
        let processedFolderPath = runtimeModule.getCurrentScript().getParameter({ name: bwp_csv_params_1.CUSTSCRIPT_BWP_PROCESSEDFOLDERPATH });
        let csvIntegrator = new bwp_csv_integrator_1.CsvIntegrator({ hostKey, url, username, secret, hostKeyType, port }, mappingId, processedFolderPath);
        csvIntegrator.run();
    };
    exports.execute = execute;
});
// Demander à Christophe si création de record > déplacement dans le SFTP car nécessite permission en écriture sur le SFTP
