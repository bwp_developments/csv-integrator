/**
 * @NApiVersion 2.1
 */
define(["require", "exports"], function (require, exports) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.CUSTSCRIPT_BWP_PROCESSEDFOLDERPATH = exports.CUSTSCRIPT_BWP_MAPPING_ID = exports.CUSTSCRIPT_BWP_SFTP_PORT = exports.CUSTSCRIPT_BWP_SFTP_HOSTKEYTYPE = exports.CUSTSCRIPT_BWP_SFTP_SECRET = exports.CUSTSCRIPT_BWP_SFTP_USERNAME = exports.CUSTSCRIPT_BWP_SFTP_URL = exports.CUSTSCRIPT_BWP_SFTP_HOSTKEY = void 0;
    exports.CUSTSCRIPT_BWP_SFTP_HOSTKEY = 'custscript_bwp_sftp_hostkey';
    exports.CUSTSCRIPT_BWP_SFTP_URL = 'custscript_bwp_sftp_url';
    exports.CUSTSCRIPT_BWP_SFTP_USERNAME = 'custscript_bwp_sftp_username';
    exports.CUSTSCRIPT_BWP_SFTP_SECRET = 'custscript_bwp_sftp_secret';
    exports.CUSTSCRIPT_BWP_SFTP_HOSTKEYTYPE = 'custscript_bwp_sftp_hostkeytype';
    exports.CUSTSCRIPT_BWP_SFTP_PORT = 'custscript_bwp_sftp_port';
    exports.CUSTSCRIPT_BWP_MAPPING_ID = 'custscript_bwp_mapping_id';
    exports.CUSTSCRIPT_BWP_PROCESSEDFOLDERPATH = 'custscript_bwp_processedfolderpath';
});
