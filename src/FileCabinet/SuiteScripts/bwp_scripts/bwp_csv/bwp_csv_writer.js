/**
 * @NApiVersion 2.1
 */
define(["require", "exports", "./bwp_csv_handler"], function (require, exports, bwp_csv_handler_1) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.CsvWriter = void 0;
    class CsvWriter extends bwp_csv_handler_1.CsvHandler {
        constructor() {
            super(...arguments);
            this.storedLength = 0;
        }
        get width() {
            return this.storedHeader.length;
        }
        get length() {
            return this.storedLength;
        }
        set header(header) {
            if (this.storedLength === 0) {
                this.writeLine(header.join(this.separator));
                this.storedHeader = header;
            }
            else {
                throw new Error('Header already set.');
            }
        }
        get header() {
            return this.storedHeader;
        }
        writeLine(value) {
            this.file.appendLine({ value });
            this.storedLength++;
        }
        insertLine(line) {
            let lengthDiff = this.width - line.length;
            if (lengthDiff > 0) {
                let padding = Array.from({ length: lengthDiff }).fill('');
                line = line.concat(padding);
            }
            else if (lengthDiff < 0) {
                throw new Error(`Line to insert ${JSON.stringify(line)} is ${-1 * lengthDiff} column(s) wider than header ${JSON.stringify(this.header)}`);
            }
            this.writeLine(line.map(cell => `"${cell}"`).join(this.separator));
        }
    }
    exports.CsvWriter = CsvWriter;
});
