/**
 * @NApiVersion 2.1
 */
define(["require", "exports", "N/sftp", "N/task", "../bwp_utils/bwp_file_system_module"], function (require, exports, sftpModule, taskModule, fileSystemModule) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.CsvIntegrator = void 0;
    class CsvIntegrator {
        constructor(sftpParams, mappingId, processedFolderPath) {
            this.sftpParams = sftpParams;
            this.mappingId = mappingId;
            this.processedFolderId = fileSystemModule.findFolderIdByPath(processedFolderPath);
        }
        run() {
            this.sftpConnection = sftpModule.createConnection(this.sftpParams);
            let remoteFilesMetaData = this.sftpConnection.list({
                path: '',
                sort: sftpModule.Sort.NAME
            });
            remoteFilesMetaData
                .map(remoteFileMetaData => remoteFileMetaData.name)
                .filter(filename => this.filterByFileName(filename))
                .map(filename => this.sftpConnection.download({ filename }))
                .map(file => this.preprocess(file))
                .reduce((flattenedFiles, filesToFlatten) => flattenedFiles.concat(filesToFlatten))
                .forEach(file => this.process(file));
        }
        filterByFileName(_fileName) {
            return true;
        }
        preprocess(file) {
            return [file];
        }
        process(file) {
            file.folder = this.processedFolderId;
            file.save();
            let task = taskModule.create({
                taskType: taskModule.TaskType.CSV_IMPORT,
                importFile: file,
                mappingId: this.mappingId
            });
            task.submit();
        }
    }
    exports.CsvIntegrator = CsvIntegrator;
});
