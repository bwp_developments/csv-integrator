/**
 * @NApiVersion 2.1
 */
define(["require", "exports", "../bwp_csv_writer"], function (require, exports, bwp_csv_writer_1) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.JournalEntryCsvWriter = void 0;
    class JournalEntryCsvWriter extends bwp_csv_writer_1.CsvWriter {
        constructor(file, separator, order, tranIdColIndex, creditColIndex, debitColIndex, defaultLine) {
            super(file, separator);
            this.order = order;
            this.tranIdColIndex = tranIdColIndex;
            this.creditColIndex = creditColIndex;
            this.debitColIndex = debitColIndex;
            this.defaultLine = defaultLine;
            this.totalCredit = 0;
            this.totalDebit = 0;
        }
        insertLine(cols) {
            this.totalCredit = parseFloat((this.totalCredit + parseFloat(cols[this.creditColIndex])).toFixed(2));
            this.totalDebit = parseFloat((this.totalDebit + parseFloat(cols[this.debitColIndex])).toFixed(2));
            let copyCols = [...cols];
            if (this.order > 0)
                copyCols[this.tranIdColIndex] += ("_#" + this.order);
            super.insertLine(copyCols);
        }
        insertEquilibriumEndLine() {
            let debit = Math.max(0, parseFloat((this.totalCredit - this.totalDebit).toFixed(2))), credit = Math.max(0, parseFloat((this.totalDebit - this.totalCredit).toFixed(2)));
            this.insertEquilibriumLine(credit, debit);
            return { credit, debit };
        }
        insertEquilibriumLine(credit, debit) {
            let lineToInsert = this.defaultLine;
            lineToInsert[this.creditColIndex] = credit.toFixed(2);
            lineToInsert[this.debitColIndex] = debit.toFixed(2);
            this.insertLine(lineToInsert);
        }
    }
    exports.JournalEntryCsvWriter = JournalEntryCsvWriter;
});
