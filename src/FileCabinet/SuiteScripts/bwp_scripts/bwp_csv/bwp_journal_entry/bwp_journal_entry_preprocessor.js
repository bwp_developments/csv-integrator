/**
 * @NApiVersion 2.1
 */
define(["require", "exports", "N/file", "../bwp_csv_reader", "./bwp_journal_entry_csv_writer"], function (require, exports, fileModule, bwp_csv_reader_1, bwp_journal_entry_csv_writer_1) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.JournalEntryPreprocessor = void 0;
    class JournalEntryPreprocessor {
        constructor(file, separator, maxLines, tranIdColIndex, creditColIndex, debitColIndex, defaultLine) {
            this.file = file;
            this.separator = separator;
            this.maxLines = maxLines;
            this.tranIdColIndex = tranIdColIndex;
            this.creditColIndex = creditColIndex;
            this.debitColIndex = debitColIndex;
            this.defaultLine = defaultLine;
            this.csvWriters = [];
            this.csvReader = new bwp_csv_reader_1.CsvReader(file, separator);
            this.inputFileName = this.file.name.replace('.csv', '');
            // Default line initialization
            this.csvReader.run([{ start: 1, handler: (reader, line) => this.extractFirstLine(reader, line) }]);
            for (let i = 0; i < defaultLine.length; i++) {
                if (defaultLine[i] === null) {
                    defaultLine[i] = this.firstLine[i];
                }
            }
        }
        get currentCsvWriter() {
            return this.csvWriters.length > 0 ? this.csvWriters[this.csvWriters.length - 1] : null;
        }
        set currentCsvWriter(csvWriter) {
            this.csvWriters.push(csvWriter);
        }
        extractFirstLine(_reader, line) {
            this.firstLine = line;
            return false;
        }
        buildAndInitCsvWriter() {
            let firstFile = fileModule.create({
                fileType: fileModule.Type.CSV,
                name: `${this.inputFileName}_${this.csvWriters.length}.csv`
            });
            let csvWriter = new bwp_journal_entry_csv_writer_1.JournalEntryCsvWriter(firstFile, this.separator, this.csvWriters.length, this.tranIdColIndex, this.creditColIndex, this.debitColIndex, this.defaultLine);
            csvWriter.header = this.csvReader.header;
            this.currentCsvWriter = csvWriter;
        }
        copyLine(_reader, line) {
            if (this.currentCsvWriter.length === this.maxLines - 1) {
                let previousValues = this.currentCsvWriter.insertEquilibriumEndLine();
                this.buildAndInitCsvWriter();
                this.currentCsvWriter.insertEquilibriumLine(previousValues.debit, previousValues.credit);
            }
            this.currentCsvWriter.insertLine(line);
            return true;
        }
        run() {
            this.buildAndInitCsvWriter();
            this.csvReader.run([{ start: 1, handler: (reader, line) => this.copyLine(reader, line) }]);
        }
    }
    exports.JournalEntryPreprocessor = JournalEntryPreprocessor;
});
