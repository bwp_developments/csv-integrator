/**
 * @NApiVersion 2.1
 */
define(["require", "exports", "../bwp_csv_integrator", "../../bwp_utils/bwp_file_system_module"], function (require, exports, bwp_csv_integrator_1, fileSystemModule) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.JournalEntryCsvIntegrator = void 0;
    class JournalEntryCsvIntegrator extends bwp_csv_integrator_1.CsvIntegrator {
        constructor(sftpParams, mappingId, processedFolderPath, preprocessorFactory) {
            super(sftpParams, mappingId, processedFolderPath);
            this.preprocessorFactory = preprocessorFactory;
            this.processedFilesNames = fileSystemModule.findAllFilesNamesByFolder(this.processedFolderId);
        }
        filterByFileName(fileName) {
            return !(this.processedFilesNames.includes(fileName));
        }
        markFileAsProcessed(file) {
            file.folder = this.processedFolderId;
            file.save();
        }
        preprocess(file) {
            let journalEntryPreprocessor = this.preprocessorFactory.create(file);
            journalEntryPreprocessor.run();
            this.markFileAsProcessed(file);
            return journalEntryPreprocessor.csvWriters.map(csvWriter => csvWriter.file);
        }
    }
    exports.JournalEntryCsvIntegrator = JournalEntryCsvIntegrator;
});
