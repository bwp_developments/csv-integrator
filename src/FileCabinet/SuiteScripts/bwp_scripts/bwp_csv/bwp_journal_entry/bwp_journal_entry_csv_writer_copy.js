/**
 * @NApiVersion 2.1
 */
define(["require", "exports", "../bwp_csv_writer"], function (require, exports, bwp_csv_writer_1) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.JournalEntryCsvWriter = void 0;
    class JournalEntryCsvWriter extends bwp_csv_writer_1.CsvWriter {
        constructor(file, separator, creditColName, debitColName, defaultLine) {
            super(file, separator);
            this.creditColName = creditColName;
            this.debitColName = debitColName;
            this.defaultLine = defaultLine;
            this.totalCredit = 0;
            this.totalDebit = 0;
        }
        insertLine(map) {
            this.totalCredit = parseFloat((this.totalCredit + parseFloat(map[this.creditColName])).toFixed(2));
            this.totalDebit = parseFloat((this.totalDebit + parseFloat(map[this.debitColName])).toFixed(2));
            super.insertLine(map);
        }
        insertEquilibriumEndLine() {
            let debit = Math.max(0, parseFloat((this.totalCredit - this.totalDebit).toFixed(2))), credit = Math.max(0, parseFloat((this.totalDebit - this.totalCredit).toFixed(2)));
            this.insertEquilibriumLine(credit, debit);
            return { credit, debit };
        }
        insertEquilibriumLine(credit, debit) {
            let lineToInsert = { ...this.defaultLine };
            lineToInsert[this.creditColName] = credit.toFixed(2);
            lineToInsert[this.debitColName] = debit.toFixed(2);
            this.insertLine(lineToInsert);
        }
    }
    exports.JournalEntryCsvWriter = JournalEntryCsvWriter;
});
