/**
 * @NApiVersion 2.1
 */
define(["require", "exports", "./bwp_journal_entry_preprocessor"], function (require, exports, bwp_journal_entry_preprocessor_1) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.JournalEntryPreprocessorFactory = void 0;
    class JournalEntryPreprocessorFactory {
        constructor(separator, maxLines, tranIdColIndex, creditColIndex, debitColIndex, defaultLine) {
            this.separator = separator;
            this.maxLines = maxLines;
            this.tranIdColIndex = tranIdColIndex;
            this.creditColIndex = creditColIndex;
            this.debitColIndex = debitColIndex;
            this.defaultLine = defaultLine;
        }
        create(file) {
            return new bwp_journal_entry_preprocessor_1.JournalEntryPreprocessor(file, this.separator, this.maxLines, this.tranIdColIndex, this.creditColIndex, this.debitColIndex, this.defaultLine);
        }
    }
    exports.JournalEntryPreprocessorFactory = JournalEntryPreprocessorFactory;
});
