/**
 * @NApiVersion 2.1
 */
define(["require", "exports", "../bwp_csv_writer"], function (require, exports, bwp_csv_writer_1) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.JournalEntryNamedCsvWriter = void 0;
    class JournalEntryNamedCsvWriter extends bwp_csv_writer_1.CsvWriter {
        constructor(file, separator, creditColName, debitColName, defaultLine) {
            super(file, separator);
            this.creditColName = creditColName;
            this.debitColName = debitColName;
            this.defaultLine = defaultLine;
            this.totalCredit = 0;
            this.totalDebit = 0;
        }
        insertLine(cols) {
            this.totalCredit = parseFloat((this.totalCredit + parseFloat(cols[this.header.indexOf(this.creditColName)])).toFixed(2));
            this.totalDebit = parseFloat((this.totalDebit + parseFloat(cols[this.header.indexOf(this.debitColName)])).toFixed(2));
            super.insertLine(cols);
        }
        insertValues(map) {
            let line = Array.from({ length: this.width }).fill('');
            for (let colName of Object.keys(map)) {
                line[this.header.indexOf(colName)] = `"${map[colName]}"`;
            }
            this.insertLine(line);
        }
        insertEquilibriumEndLine() {
            let debit = Math.max(0, parseFloat((this.totalCredit - this.totalDebit).toFixed(2))), credit = Math.max(0, parseFloat((this.totalDebit - this.totalCredit).toFixed(2)));
            this.insertEquilibriumLine(credit, debit);
            return { credit, debit };
        }
        insertEquilibriumLine(credit, debit) {
            let lineToInsert = { ...this.defaultLine };
            lineToInsert[this.creditColName] = credit.toFixed(2);
            lineToInsert[this.debitColName] = debit.toFixed(2);
            this.insertValues(lineToInsert);
        }
    }
    exports.JournalEntryNamedCsvWriter = JournalEntryNamedCsvWriter;
});
