/**
 * @NApiVersion 2.1
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(["require", "exports", "N/runtime", "./bwp_journal_entry_csv_integrator", "./bwp_journal_entry_csv_params", "./bwp_journal_entry_preprocessor_factory"], function (require, exports, runtimeModule, bwp_journal_entry_csv_integrator_1, bwp_journal_entry_csv_params_1, bwp_journal_entry_preprocessor_factory_1) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.execute = void 0;
    let execute = (_scriptContext) => {
        // SFTP params
        let hostKey = runtimeModule.getCurrentScript().getParameter({ name: bwp_journal_entry_csv_params_1.CUSTSCRIPT_BWP_SFTP_HOSTKEY });
        let url = runtimeModule.getCurrentScript().getParameter({ name: bwp_journal_entry_csv_params_1.CUSTSCRIPT_BWP_SFTP_URL });
        let username = runtimeModule.getCurrentScript().getParameter({ name: bwp_journal_entry_csv_params_1.CUSTSCRIPT_BWP_SFTP_USERNAME });
        let secret = runtimeModule.getCurrentScript().getParameter({ name: bwp_journal_entry_csv_params_1.CUSTSCRIPT_BWP_SFTP_SECRET });
        let hostKeyType = runtimeModule.getCurrentScript().getParameter({ name: bwp_journal_entry_csv_params_1.CUSTSCRIPT_BWP_SFTP_HOSTKEYTYPE });
        let port = runtimeModule.getCurrentScript().getParameter({ name: bwp_journal_entry_csv_params_1.CUSTSCRIPT_BWP_SFTP_PORT });
        // Processing params
        let mappingId = runtimeModule.getCurrentScript().getParameter({ name: bwp_journal_entry_csv_params_1.CUSTSCRIPT_BWP_MAPPING_ID });
        let processedFolderPath = runtimeModule.getCurrentScript().getParameter({ name: bwp_journal_entry_csv_params_1.CUSTSCRIPT_BWP_PROCESSEDFOLDERPATH });
        let maxLines = runtimeModule.getCurrentScript().getParameter({ name: bwp_journal_entry_csv_params_1.CUSTSCRIPT_BWP_MAXLINES });
        let defaultLine = JSON.parse(runtimeModule.getCurrentScript().getParameter({ name: bwp_journal_entry_csv_params_1.CUSTSCRIPT_BWP_JSONDEFAULTLINE }));
        // Parsing params
        let separator = runtimeModule.getCurrentScript().getParameter({ name: bwp_journal_entry_csv_params_1.CUSTSCRIPT_BWP_SEPARATOR });
        let tranIdColIndex = runtimeModule.getCurrentScript().getParameter({ name: bwp_journal_entry_csv_params_1.CUSTSCRIPT_BWP_TRANIDCOLINDEX });
        let creditColIndex = runtimeModule.getCurrentScript().getParameter({ name: bwp_journal_entry_csv_params_1.CUSTSCRIPT_BWP_CREDITCOLINDEX });
        let debitColIndex = runtimeModule.getCurrentScript().getParameter({ name: bwp_journal_entry_csv_params_1.CUSTSCRIPT_BWP_DEBITCOLINDEX });
        let preprocessorFactory = new bwp_journal_entry_preprocessor_factory_1.JournalEntryPreprocessorFactory(separator, maxLines, tranIdColIndex, creditColIndex, debitColIndex, defaultLine);
        let csvIntegrator = new bwp_journal_entry_csv_integrator_1.JournalEntryCsvIntegrator({ hostKey, url, username, secret, hostKeyType, port }, mappingId, processedFolderPath, preprocessorFactory);
        csvIntegrator.run();
    };
    exports.execute = execute;
});
