/**
 * @NApiVersion 2.1
 */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
define(["require", "exports", "../bwp_csv_params"], function (require, exports, bwp_csv_params_1) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.CUSTSCRIPT_BWP_JSONDEFAULTLINE = exports.CUSTSCRIPT_BWP_DEBITCOLINDEX = exports.CUSTSCRIPT_BWP_CREDITCOLINDEX = exports.CUSTSCRIPT_BWP_TRANIDCOLINDEX = exports.CUSTSCRIPT_BWP_MAXLINES = exports.CUSTSCRIPT_BWP_SEPARATOR = exports.CUSTSCRIPT_BWP_PROCESSEDFOLDERPATH = void 0;
    __exportStar(bwp_csv_params_1, exports);
    exports.CUSTSCRIPT_BWP_PROCESSEDFOLDERPATH = 'custscript_bwp_processedfolderpath';
    exports.CUSTSCRIPT_BWP_SEPARATOR = 'custscript_bwp_separator';
    exports.CUSTSCRIPT_BWP_MAXLINES = 'custscript_bwp_maxlines';
    exports.CUSTSCRIPT_BWP_TRANIDCOLINDEX = 'custscript_bwp_tranidcolindex';
    exports.CUSTSCRIPT_BWP_CREDITCOLINDEX = 'custscript_bwp_creditcolindex';
    exports.CUSTSCRIPT_BWP_DEBITCOLINDEX = 'custscript_bwp_debitcolindex';
    exports.CUSTSCRIPT_BWP_JSONDEFAULTLINE = 'custscript_bwp_jsondefaultline';
});
