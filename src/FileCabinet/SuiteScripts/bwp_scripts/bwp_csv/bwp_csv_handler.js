/**
 * @NApiVersion 2.1
 */
define(["require", "exports"], function (require, exports) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.CsvHandler = void 0;
    class CsvHandler {
        constructor(file, separator) {
            this.file = file;
            this.separator = separator;
        }
    }
    exports.CsvHandler = CsvHandler;
});
