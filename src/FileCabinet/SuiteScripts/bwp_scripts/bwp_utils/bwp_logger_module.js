/**
 * @NApiVersion 2.1
 */
define(["require", "exports", "N/log"], function (require, exports, log) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.verbose = void 0;
    function verbose() {
        return function (_target, propertyKey, descriptor) {
            const targetMethod = descriptor.value;
            descriptor.value = function (...args) {
                log.audit({
                    title: 'Call',
                    details: propertyKey + JSON.stringify(args).replace('[', '(').replace(']', ')')
                });
                return targetMethod.apply(this, args);
            };
            return descriptor;
        };
    }
    exports.verbose = verbose;
});
