/**
 * @NApiVersion 2.1
 */
define(["require", "exports"], function (require, exports) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.isMap = exports.isArray = void 0;
    function isArray(obj) {
        return getType(obj) === 'Array';
    }
    exports.isArray = isArray;
    function isMap(obj) {
        return getType(obj) === 'Map';
    }
    exports.isMap = isMap;
    function getType(obj) {
        return Object.prototype.toString.call(obj).slice(8, -1);
    }
});
