/**
 * @NApiVersion 2.1
 */

import { File } from 'N/file';

import { CsvHandler } from './bwp_csv_handler';

const quotes = ['`', '"', '\''];

export class CsvReader extends CsvHandler {
    readonly header: string[];
    private storedLength: number = -1;

    constructor(file: File, separator: string) {
        super(file, separator);
        let header;
        file.lines.iterator().each(({ value }: { value: string }) => {
            header = this.splitWithQuotes(value);
            return false;
        });
        this.header = header;
    }

    get width() {
        return this.header.length;
    }

    get length() {
        if (this.storedLength < 0) {
            this.storedLength = 0;
            this.file.lines.iterator().each(_line => {
                this.storedLength++;
                return true;
            });
        }
        return this.storedLength;
    }

    private splitWithQuotes(line: string) {
        let values: string[] = [];
        let currentValue = '';
        let currentQuote = null;
        for (let i = 0; i < line.length; i++) {
            let char = line.charAt(i);
            if (currentValue.length === 0) {
                if (quotes.includes(char)) {
                    currentQuote = char;
                } else if (char === this.separator) {
                    values.push(currentValue);
                    currentValue = '';
                } else {
                    currentValue += char;
                }
            } else {
                if (char === currentQuote) {
                    currentQuote = null;
                } else if (!currentQuote && char === this.separator) {
                    values.push(currentValue);
                    currentValue = '';
                } else {
                    currentValue += char;
                }
            }
        }
        values.push(currentValue);
        return values;
    }

    getValue(colName: string, line: string[]) {
        let cell = line[this.header.indexOf(colName)];
        if (cell.startsWith('"') && cell.endsWith('"')) {
            cell = cell.substring(1, cell.length - 1);
        }
        return cell;
    }

    run(lineHandlers: {start?: number, end?: number, handler: ((reader: CsvReader, line: string[]) => boolean)}[]) {
        let n = 0;
        let continueFlags = Array.from<boolean>({ length: lineHandlers.length }).fill(true);

        this.file.lines.iterator().each(({ value }: { value: string }) => {
            for (let i = 0; i < lineHandlers.length; i++) {
                let lineHandler = lineHandlers[i];
                if (continueFlags[i]
                    && (!lineHandler.start || n >= lineHandler.start)
                    && (!lineHandler.end || n < lineHandler.end)) {
                    continueFlags[i] = lineHandler.handler(this, this.splitWithQuotes(value));
                }
            }
            n++;
            return continueFlags.reduce((prev, curr) => prev || curr);
        });
    }
}
