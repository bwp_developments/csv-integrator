/**
 * @NApiVersion 2.1
 */

import { CsvHandler } from './bwp_csv_handler';

export class CsvWriter extends CsvHandler {
    private storedHeader: string[];
    private storedLength: number = 0;

    get width() {
        return this.storedHeader.length;
    }
    get length() {
        return this.storedLength;
    }

    set header(header: string[]) {
        if (this.storedLength === 0) {
            this.writeLine(header.join(this.separator));
            this.storedHeader = header;
        } else {
            throw new Error('Header already set.');
        }
    }
    get header() {
        return this.storedHeader;
    }

    private writeLine(value: string) {
        this.file.appendLine({ value });
        this.storedLength++;
    }

    insertLine(line: string[]) {
        let lengthDiff = this.width - line.length;
        if (lengthDiff > 0) {
            let padding = Array.from<string>({ length: lengthDiff }).fill('');
            line = line.concat(padding);
        } else if (lengthDiff < 0) {
            throw new Error(`Line to insert ${JSON.stringify(line)} is ${-1 * lengthDiff} column(s) wider than header ${JSON.stringify(this.header)}`);
        }
        this.writeLine(line.map(cell => `"${cell}"`).join(this.separator));
    }
}