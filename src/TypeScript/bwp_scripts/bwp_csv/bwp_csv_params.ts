/**
 * @NApiVersion 2.1
 */

export const CUSTSCRIPT_BWP_SFTP_HOSTKEY = 'custscript_bwp_sftp_hostkey';
export const CUSTSCRIPT_BWP_SFTP_URL = 'custscript_bwp_sftp_url';
export const CUSTSCRIPT_BWP_SFTP_USERNAME = 'custscript_bwp_sftp_username';
export const CUSTSCRIPT_BWP_SFTP_SECRET = 'custscript_bwp_sftp_secret';
export const CUSTSCRIPT_BWP_SFTP_HOSTKEYTYPE = 'custscript_bwp_sftp_hostkeytype';
export const CUSTSCRIPT_BWP_SFTP_PORT = 'custscript_bwp_sftp_port';

export const CUSTSCRIPT_BWP_MAPPING_ID = 'custscript_bwp_mapping_id';

export const CUSTSCRIPT_BWP_PROCESSEDFOLDERPATH = 'custscript_bwp_processedfolderpath';