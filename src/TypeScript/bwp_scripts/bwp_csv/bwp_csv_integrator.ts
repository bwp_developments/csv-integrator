/**
 * @NApiVersion 2.1
 */

import * as sftpModule from 'N/sftp';
import * as taskModule from 'N/task';

import * as fileSystemModule from '../bwp_utils/bwp_file_system_module';

import { File } from 'N/file';

export class CsvIntegrator {
    private sftpConnection: sftpModule.Connection;
    protected processedFolderId: number;

    constructor(readonly sftpParams: sftpModule.CreateSFTPConnectionWithSecretOptions, readonly mappingId: string,
        processedFolderPath: string) {
        this.processedFolderId = fileSystemModule.findFolderIdByPath(processedFolderPath);
    }

    run() {
        this.sftpConnection = sftpModule.createConnection(this.sftpParams);

        let remoteFilesMetaData = this.sftpConnection.list({
            path: '',
            sort: sftpModule.Sort.NAME
        });

        remoteFilesMetaData
            .map(remoteFileMetaData => remoteFileMetaData.name)
            .filter(filename => this.filterByFileName(filename))
            .map(filename => this.sftpConnection.download({ filename }))
            .map(file => this.preprocess(file))
            .reduce((flattenedFiles, filesToFlatten) => flattenedFiles.concat(filesToFlatten))
            .forEach(file => this.process(file));
    }

    filterByFileName(_fileName: string) {
        return true;
    }

    preprocess(file: File) {
        return [file];
    }

    process(file: File) {
        file.folder = this.processedFolderId;
        file.save();
        let task = taskModule.create({
            taskType: taskModule.TaskType.CSV_IMPORT,
            importFile: file,
            mappingId: this.mappingId
        });
        task.submit();
    }
}