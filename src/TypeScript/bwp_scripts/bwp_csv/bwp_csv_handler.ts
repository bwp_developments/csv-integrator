/**
 * @NApiVersion 2.1
 */

import { File } from 'N/file';

export abstract class CsvHandler {
    constructor(readonly file: File, readonly separator: string) {}

    abstract get width();
    abstract get length();
}