/**
 * @NApiVersion 2.1
 */

import { File } from 'N/file';

import * as fileModule from 'N/file';

import { CsvReader } from "../bwp_csv_reader";
import { JournalEntryCsvWriter } from './bwp_journal_entry_csv_writer';

export class JournalEntryPreprocessor {
    csvWriters: JournalEntryCsvWriter[] = [];
    private csvReader: CsvReader;
    private inputFileName: string;
    private firstLine: string[];

    constructor(readonly file: File, readonly separator: string, readonly maxLines: number, readonly tranIdColIndex: number,
        readonly creditColIndex: number, readonly debitColIndex: number, readonly defaultLine: string[]) {
        this.csvReader = new CsvReader(file, separator);
        this.inputFileName = this.file.name.replace('.csv', '');
        // Default line initialization
        this.csvReader.run([{ start: 1, handler: (reader, line) => this.extractFirstLine(reader, line) }]);
        for (let i = 0; i < defaultLine.length; i++) {
            if (defaultLine[i] === null) {
                defaultLine[i] = this.firstLine[i];
            }
        }
    }

    get currentCsvWriter() {
        return this.csvWriters.length > 0 ? this.csvWriters[this.csvWriters.length - 1] : null;
    }
    set currentCsvWriter(csvWriter: JournalEntryCsvWriter) {
        this.csvWriters.push(csvWriter);
    }

    private extractFirstLine(_reader: CsvReader, line: string[]) {
        this.firstLine = line;
        return false;
    }

    private buildAndInitCsvWriter() {
        let firstFile = fileModule.create({
            fileType: fileModule.Type.CSV,
            name: `${this.inputFileName}_${this.csvWriters.length}.csv`
        });
        let csvWriter = new JournalEntryCsvWriter(firstFile, this.separator, this.csvWriters.length,
            this.tranIdColIndex, this.creditColIndex, this.debitColIndex, this.defaultLine);
        csvWriter.header = this.csvReader.header;

        this.currentCsvWriter = csvWriter;
    }

    copyLine(_reader: CsvReader, line: string[]) {
        if (this.currentCsvWriter.length === this.maxLines - 1) {
            let previousValues = this.currentCsvWriter.insertEquilibriumEndLine();

            this.buildAndInitCsvWriter();

            this.currentCsvWriter.insertEquilibriumLine(previousValues.debit, previousValues.credit);
        }

        this.currentCsvWriter.insertLine(line);

        return true;
    }

    run() {
        this.buildAndInitCsvWriter();

        this.csvReader.run([{ start: 1, handler: (reader, line) => this.copyLine(reader, line) }]);
    }
}
