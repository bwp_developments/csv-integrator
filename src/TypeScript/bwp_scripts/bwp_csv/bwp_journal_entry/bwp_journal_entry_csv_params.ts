/**
 * @NApiVersion 2.1
 */

export * from "../bwp_csv_params";

export const CUSTSCRIPT_BWP_SEPARATOR = 'custscript_bwp_separator';
export const CUSTSCRIPT_BWP_MAXLINES = 'custscript_bwp_maxlines';
export const CUSTSCRIPT_BWP_TRANIDCOLINDEX = 'custscript_bwp_tranidcolindex';
export const CUSTSCRIPT_BWP_CREDITCOLINDEX = 'custscript_bwp_creditcolindex';
export const CUSTSCRIPT_BWP_DEBITCOLINDEX = 'custscript_bwp_debitcolindex';
export const CUSTSCRIPT_BWP_JSONDEFAULTLINE = 'custscript_bwp_jsondefaultline';