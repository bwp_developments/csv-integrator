/**
 * @NApiVersion 2.1
 */

import { File } from 'N/file';

import { CsvWriter } from "../bwp_csv_writer";

export class JournalEntryNamedCsvWriter extends CsvWriter {
    private totalCredit = 0;
    private totalDebit = 0;

    constructor(file: File, separator: string, readonly creditColName: string,
        readonly debitColName: string, readonly defaultLine: { [colName: string]: string; }) {
        super(file, separator);
    }

    insertLine(cols: string[]) {
        this.totalCredit = parseFloat((this.totalCredit + parseFloat(cols[this.header.indexOf(this.creditColName)])).toFixed(2));
        this.totalDebit = parseFloat((this.totalDebit + parseFloat(cols[this.header.indexOf(this.debitColName)])).toFixed(2));
        super.insertLine(cols);
    }

    insertValues(map: {[colName: string]: string}) {
        let line = Array.from<string>({ length: this.width }).fill('');
        for (let colName of Object.keys(map)) {
            line[this.header.indexOf(colName)] = `"${map[colName]}"`;
        }
        this.insertLine(line);
    }

    insertEquilibriumEndLine() {
        let debit = Math.max(0, parseFloat((this.totalCredit - this.totalDebit).toFixed(2))),
            credit = Math.max(0, parseFloat((this.totalDebit - this.totalCredit).toFixed(2)));
        this.insertEquilibriumLine(credit, debit);
        return { credit, debit };
    }

    insertEquilibriumLine(credit: number, debit: number) {         
        let lineToInsert = { ...this.defaultLine };
        lineToInsert[this.creditColName] = credit.toFixed(2);
        lineToInsert[this.debitColName] = debit.toFixed(2);
        this.insertValues(lineToInsert);
    }
}