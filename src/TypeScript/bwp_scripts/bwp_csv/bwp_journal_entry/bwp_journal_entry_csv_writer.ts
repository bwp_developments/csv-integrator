/**
 * @NApiVersion 2.1
 */

import { File } from 'N/file';

import { CsvWriter } from "../bwp_csv_writer";

export class JournalEntryCsvWriter extends CsvWriter {
    private totalCredit = 0;
    private totalDebit = 0;

    constructor(file: File, separator: string, readonly order: number, readonly tranIdColIndex: number,
        readonly creditColIndex: number, readonly debitColIndex: number, readonly defaultLine: string[]) {
        super(file, separator);
    }

    insertLine(cols: string[]) {
        this.totalCredit = parseFloat((this.totalCredit + parseFloat(cols[this.creditColIndex])).toFixed(2));
        this.totalDebit = parseFloat((this.totalDebit + parseFloat(cols[this.debitColIndex])).toFixed(2));
        let copyCols = [...cols];
        if (this.order > 0)
            copyCols[this.tranIdColIndex] += ("_#" + this.order);
        super.insertLine(copyCols);
    }

    insertEquilibriumEndLine() {
        let debit = Math.max(0, parseFloat((this.totalCredit - this.totalDebit).toFixed(2))),
            credit = Math.max(0, parseFloat((this.totalDebit - this.totalCredit).toFixed(2)));
        this.insertEquilibriumLine(credit, debit);
        return { credit, debit };
    }

    insertEquilibriumLine(credit: number, debit: number) {         
        let lineToInsert = this.defaultLine;
        lineToInsert[this.creditColIndex] = credit.toFixed(2);
        lineToInsert[this.debitColIndex] = debit.toFixed(2);
        this.insertLine(lineToInsert);
    }
}