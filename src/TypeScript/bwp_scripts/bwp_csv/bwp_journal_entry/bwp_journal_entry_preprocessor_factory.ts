/**
 * @NApiVersion 2.1
 */

import { File } from '@hitc/netsuite-types/N/file';

import { JournalEntryPreprocessor } from "./bwp_journal_entry_preprocessor"

export class JournalEntryPreprocessorFactory {
    constructor(readonly separator: string, readonly maxLines: number, readonly tranIdColIndex: number,
        readonly creditColIndex: number, readonly debitColIndex: number, readonly defaultLine: string[]) { }

    create(file: File) {
        return new JournalEntryPreprocessor(file, this.separator, this.maxLines, this.tranIdColIndex,
            this.creditColIndex, this.debitColIndex, this.defaultLine);
    }
}
