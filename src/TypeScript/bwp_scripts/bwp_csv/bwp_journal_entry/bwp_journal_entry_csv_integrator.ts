/**
 * @NApiVersion 2.1
 */

import type { File } from 'N/file';
import type { CreateSFTPConnectionWithSecretOptions } from 'N/sftp';

import { CsvIntegrator } from '../bwp_csv_integrator';
import * as fileSystemModule from '../../bwp_utils/bwp_file_system_module';
import { JournalEntryPreprocessorFactory } from './bwp_journal_entry_preprocessor_factory';

export class JournalEntryCsvIntegrator extends CsvIntegrator {

    private processedFilesNames: string[];

    constructor(sftpParams: CreateSFTPConnectionWithSecretOptions, mappingId: string,
        processedFolderPath: string, readonly preprocessorFactory: JournalEntryPreprocessorFactory) {
        super(sftpParams, mappingId, processedFolderPath);
        this.processedFilesNames = fileSystemModule.findAllFilesNamesByFolder(this.processedFolderId);
    }

    filterByFileName(fileName: string): boolean {
        return !(this.processedFilesNames.includes(fileName));
    }

    private markFileAsProcessed(file: File) {
        file.folder = this.processedFolderId;
        file.save();
    }

    preprocess(file: File): File[] {
        let journalEntryPreprocessor = this.preprocessorFactory.create(file);
        journalEntryPreprocessor.run();

        this.markFileAsProcessed(file);

        return journalEntryPreprocessor.csvWriters.map(csvWriter => csvWriter.file);
    }
}