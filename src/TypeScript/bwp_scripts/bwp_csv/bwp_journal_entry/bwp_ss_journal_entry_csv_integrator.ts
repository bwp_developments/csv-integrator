/**
 * @NApiVersion 2.1
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

import { EntryPoints } from "N/types";

import * as runtimeModule from 'N/runtime';
import * as log from 'N/log';

import { JournalEntryCsvIntegrator } from "./bwp_journal_entry_csv_integrator";
import { 
    CUSTSCRIPT_BWP_SFTP_HOSTKEY,
    CUSTSCRIPT_BWP_SFTP_URL,
    CUSTSCRIPT_BWP_SFTP_USERNAME,
    CUSTSCRIPT_BWP_SFTP_SECRET,
    CUSTSCRIPT_BWP_SFTP_HOSTKEYTYPE,
    CUSTSCRIPT_BWP_SFTP_PORT,
    CUSTSCRIPT_BWP_MAPPING_ID,
    CUSTSCRIPT_BWP_PROCESSEDFOLDERPATH,
    CUSTSCRIPT_BWP_MAXLINES,
    CUSTSCRIPT_BWP_JSONDEFAULTLINE,
    CUSTSCRIPT_BWP_SEPARATOR,
    CUSTSCRIPT_BWP_TRANIDCOLINDEX,
    CUSTSCRIPT_BWP_CREDITCOLINDEX,
    CUSTSCRIPT_BWP_DEBITCOLINDEX
} from './bwp_journal_entry_csv_params';
import { JournalEntryPreprocessorFactory } from "./bwp_journal_entry_preprocessor_factory";

export let execute: EntryPoints.Scheduled.execute = (_scriptContext) => {
    // SFTP params
    let hostKey = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_SFTP_HOSTKEY }) as string;
    let url = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_SFTP_URL }) as string;
    let username = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_SFTP_USERNAME }) as string;
    let secret = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_SFTP_SECRET }) as string;
    let hostKeyType = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_SFTP_HOSTKEYTYPE }) as 'dsa' | 'ecdsa' | 'rsa';
    let port = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_SFTP_PORT }) as number;
    // Processing params
    let mappingId = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_MAPPING_ID }) as string;
    let processedFolderPath = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_PROCESSEDFOLDERPATH }) as string;
    let maxLines = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_MAXLINES }) as number;
    let defaultLine = JSON.parse(runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_JSONDEFAULTLINE }) as string);
    // Parsing params
    let separator = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_SEPARATOR }) as string;
    let tranIdColIndex = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_TRANIDCOLINDEX }) as number;
    let creditColIndex = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_CREDITCOLINDEX }) as number;
    let debitColIndex = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_DEBITCOLINDEX }) as number;

    let preprocessorFactory = new JournalEntryPreprocessorFactory(separator, maxLines, tranIdColIndex, creditColIndex, debitColIndex, defaultLine)

    let csvIntegrator = new JournalEntryCsvIntegrator({ hostKey, url, username, secret, hostKeyType, port }, mappingId,
        processedFolderPath, preprocessorFactory);

    csvIntegrator.run();
};