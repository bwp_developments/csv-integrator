/**
 * @NApiVersion 2.1
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

import { EntryPoints } from "N/types";

import * as runtimeModule from 'N/runtime';

import { CsvIntegrator } from "./bwp_csv_integrator";
import { 
    CUSTSCRIPT_BWP_SFTP_HOSTKEY,
    CUSTSCRIPT_BWP_SFTP_URL,
    CUSTSCRIPT_BWP_SFTP_USERNAME,
    CUSTSCRIPT_BWP_SFTP_SECRET,
    CUSTSCRIPT_BWP_SFTP_HOSTKEYTYPE,
    CUSTSCRIPT_BWP_SFTP_PORT,
    CUSTSCRIPT_BWP_MAPPING_ID,
    CUSTSCRIPT_BWP_PROCESSEDFOLDERPATH
} from './bwp_csv_params';

export let execute: EntryPoints.Scheduled.execute = (_scriptContext) => {
    let hostKey = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_SFTP_HOSTKEY }) as string;
    let url = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_SFTP_URL }) as string;
    let username = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_SFTP_USERNAME }) as string;
    let secret = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_SFTP_SECRET }) as string;
    let hostKeyType = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_SFTP_HOSTKEYTYPE }) as 'dsa' | 'ecdsa' | 'rsa';
    let port = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_SFTP_PORT }) as number;

    let mappingId = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_MAPPING_ID }) as string;

    let processedFolderPath = runtimeModule.getCurrentScript().getParameter({ name: CUSTSCRIPT_BWP_PROCESSEDFOLDERPATH }) as string;

    let csvIntegrator = new CsvIntegrator({ hostKey, url, username, secret, hostKeyType, port }, mappingId, processedFolderPath);

    csvIntegrator.run();
};

// Demander à Christophe si création de record > déplacement dans le SFTP car nécessite permission en écriture sur le SFTP