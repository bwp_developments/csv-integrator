/**
 * @NApiVersion 2.1
 */

import * as queryModule from "N/query";

export function findFolderIdByPath(path: string): number {
    let foldersNames = path.split('/');
    let lastFolderNameIndex = foldersNames.length - 1;

    let lastFolderName = foldersNames[lastFolderNameIndex];
    let firstFolderName = foldersNames[0];

    let pathFilter = foldersNames.length === 1 ?
        `BUILTIN.HIERARCHY(mif.parent, 'DISPLAY') IS NULL` :
        `BUILTIN.HIERARCHY(mif.parent, 'DISPLAY') = '${foldersNames.slice(0, lastFolderNameIndex).join('/')}'`;
    let query = `SELECT mif.id
        FROM mediaitemfolder mif
        WHERE mif.name = '${lastFolderName}'
        AND ${pathFilter}
        START WITH mif.name = '${firstFolderName}'
        CONNECT BY PRIOR mif.id = mif.parent;`;

    let q = queryModule.runSuiteQL({ query });

    if (q.results.length > 1) {
        throw new Error(`${q.results.length} folders were found by path request '${query}'.`);
    } else if (q.results.length === 0) {
        throw new Error(`No folder was found by path request '${query}'.`)
    } else {
        return q.results[0].values[0] as number;
    }
}

export function findAllFilesNamesByFolder(folderId: number): string[] {
    let query = `SELECT f.name FROM File f WHERE f.folder = ${folderId}`;
    let q = queryModule.runSuiteQL({ query });
    return q.results.map(result => (result.values[0] as string));
}