/**
 * @NApiVersion 2.1
 */

import * as log from 'N/log';

export function verbose() {
    return function (_target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const targetMethod = descriptor.value;
        descriptor.value = function (...args: any[]) {
            log.audit({
                title: 'Call',
                details: propertyKey + JSON.stringify(args).replace('[', '(').replace(']', ')')
            });
            return targetMethod.apply(this, args);
        }
        return descriptor;
    }
}