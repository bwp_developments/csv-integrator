/**
 * @NApiVersion 2.1
 */

export function isArray(obj: Object) {
    return getType(obj) === 'Array';
}

export function isMap(obj: Object) {
    return getType(obj) === 'Map';
}

function getType(obj: Object) {
    return (Object.prototype.toString.call(obj) as string).slice(8, -1);
}